﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;

namespace NetworkBroadcastReciever
{
    [Activity(Label = "NetworkBroadcastReciever", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            TextView button = FindViewById<TextView>(Resource.Id.myTextView);

            button.Click += delegate {

                StartActivity(new Intent(Android.Provider.Settings.ActionWifiSettings));
            
            };
        }
    }
}

