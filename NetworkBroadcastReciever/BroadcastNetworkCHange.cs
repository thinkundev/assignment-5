﻿using Android.App;
using Android.Content;
using Android.Widget;
using System;
using Android.OS;
using Android.Net;

[BroadcastReceiver(Enabled = true, Exported = false)]
[IntentFilter(new string[] { "android.net.conn.CONNECTIVITY_CHANGE" })]

public class BroadcastNetworkCHange : BroadcastReceiver
{
        public BroadcastNetworkCHange()
        {
        }

    public override void OnReceive(Context context, Intent intent)
    {
        ConnectivityManager cm =
                (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);

        Network[] activeNetwork;
        bool isConnected = false;
        String type = "";
        if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
        {

            activeNetwork = cm.GetAllNetworks();
            NetworkInfo networkInfo;

            foreach (Network mNetwork in activeNetwork)
            {
                networkInfo = cm.GetNetworkInfo(mNetwork);

                if (networkInfo.GetState().Equals(NetworkInfo.State.Connected))
                {
                    type = networkInfo.TypeName;
                    isConnected = true;
                }
            }
        }
        if (isConnected)
        {
            try
            {
                Toast.MakeText(context, "Network is connected to " + type, ToastLength.Long).Show();
            }

            catch (Exception e)
            {
                Console.Write(e.Data);
            }
        }
        else
        {
            Toast.MakeText(context, "Network is disconnected or reconnected", ToastLength.Long).Show();
        }

    }
}
